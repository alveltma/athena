/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
//  KalmanMETCorrectionConstants.h
//  TopoCore
//  Created by Ralf Gugel on 04/11/24.

#ifndef __TopoCore__KalmanMETCorrectionConstants__
#define __TopoCore__KalmanMETCorrectionConstants__

#include <map>

namespace TCS {
   namespace KFMET { 
      // Number of menu-defined words needed for the compactified representation of the KFMET LUT values
      constexpr size_t nWeightWords {49};
      // Number of eta bins for the LUT of the KalmanMETCorrection algorithm (including one excess bin for jets not in any valid eta range)
      constexpr unsigned nEtaBins {30};
      // Number of Et bins for the LUT of the KalmanMETCorrection algorithm.
      constexpr unsigned nLogEtBins {6};
      // Bit width for the correction factors of the KalmanMETCorrection algorithm
      constexpr unsigned correctionDecimalBitWidth {7}; // 1 sign bit, 1 integer bit
      constexpr unsigned correctionBitWidth {9};
      // ignore LSBs below this bit in determining logET bin
      constexpr int jetEtBinOffset {5}; 
      // large-ish lookup table mapping Topo-internal eta coordinates to eta bin indices in the 2D KFMET LUT
      // while the former is also hardcoded in firmware, the latter is menu-configurable
      constexpr size_t lookupEtaBinFallback {nEtaBins-1};
      const std::map<unsigned, size_t> lookupEtaBin {
        {0 , 0},
        {1 , 0},
        {2 , 0},
        {3 , 0},
        {4 , 1},
        {5 , 1},
        {6 , 1},
        {7 , 1},
        {8 , 2},
        {9 , 2},
        {10 , 2},
        {11 , 2},
        {12 , 3},
        {13 , 3},
        {14 , 3},
        {15 , 3},
        {16 , 4},
        {17 , 4},
        {18 , 4},
        {19 , 4},
        {20 , 5},
        {21 , 5},
        {22 , 5},
        {23 , 5},
        {24 , 6},
        {25 , 6},
        {26 , 6},
        {27 , 6},
        {28 , 7},
        {29 , 7},
        {30 , 7},
        {31 , 7},
        {32 , 8},
        {33 , 8},
        {34 , 8},
        {35 , 8},
        {36 , 9},
        {37 , 9},
        {38 , 9},
        {39 , 9},
        {40 , 10},
        {41 , 10},
        {42 , 10},
        {43 , 10},
        {44 , 11},
        {45 , 11},
        {46 , 11},
        {47 , 11},
        {48 , 12},
        {49 , 12},
        {50 , 12},
        {51 , 12},
        {52 , 13},
        {53 , 13},
        {54 , 13},
        {55 , 13},
        {56 , 14},
        {57 , 14},
        {58 , 14},
        {59 , 14},
        {60 , 15},
        {61 , 15},
        {62 , 15},
        {63 , 15},
        {64 , 16},
        {65 , 16},
        {66 , 16},
        {67 , 16},
        {68 , 17},
        {69 , 17},
        {70 , 17},
        {71 , 17},
        {72 , 18},
        {73 , 18},
        {74 , 18},
        {75 , 18},
        {76 , 19},
        {77 , 19},
        {78 , 19},
        {79 , 19},
        {80 , 20},
        {81 , 20},
        {82 , 20},
        {83 , 20},
        {84 , 21},
        {85 , 21},
        {86 , 21},
        {87 , 21},
        {88 , 22},
        {89 , 22},
        {90 , 22},
        {91 , 22},
        {92 , 23},
        {93 , 23},
        {94 , 23},
        {95 , 23},
        {96 , 24},
        {97 , 24},
        {98 , 24},
        {99 , 24},
        {100 , 25},
        {101 , 25},
        {102 , 25},
        {103 , 25},
        {104 , 25},
        {105 , 25},
        {106 , 25},
        {107 , 25},
        {108 , 26},
        {109 , 26},
        {110 , 26},
        {111 , 26},
        {112 , 26},
        {113 , 26},
        {114 , 26},
        {115 , 26},
        {116 , 27},
        {117 , 27},
        {118 , 27},
        {119 , 27},
        {120 , 27},
        {121 , 27},
        {122 , 27},
        {123 , 27},
        {124 , 27},
        {125 , 27},
        {126 , 27},
        {127 , 27},
        {128 , 28},
        {129 , 28},
        {130 , 28},
        {131 , 28},
        {132 , 28},
        {133 , 28},
        {134 , 28},
        {135 , 28},
        {136 , 28},
        {137 , 28},
        {138 , 28},
        {139 , 28},
        {140 , 28},
        {141 , 28},
        {142 , 28},
        {143 , 28},
        {144 , 28},
        {145 , 28},
        {146 , 28},
        {147 , 28},
        {148 , 28},
        {149 , 28},
        {150 , 28},
        {151 , 28},
        {152 , 28},
        {153 , 28},
        {154 , 28},
        {155 , 28},
        {156 , 28},
        {157 , 28},
        {158 , 28},
        {159 , 28},
        {160 , 28},
        {161 , 28},
        {162 , 28},
        {163 , 28},
        {164 , 28},
        {165 , 28},
        {166 , 28},
        {167 , 28},
        {168 , 28},
        {169 , 28},
        {170 , 28},
        {171 , 28},
        {172 , 28},
        {173 , 28},
        {174 , 28},
        {175 , 28},
        {176 , 28},
        {177 , 28},
        {178 , 28},
        {179 , 28},
        {180 , 28},
        {181 , 28},
        {182 , 28},
        {183 , 28},
        {184 , 28},
        {185 , 28},
        {186 , 28},
        {187 , 28},
        {188 , 28},
        {189 , 28},
        {190 , 28},
        {191 , 28},
        {192 , 28},
        {193 , 28},
        {194 , 28},
        {195 , 28}
      };

    
   }
}

#endif
