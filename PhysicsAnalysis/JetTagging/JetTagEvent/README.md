# JetTagEvent

## Introduction
JetTagEvent contains classes to store tagging information. The classes are
used by the b-tagging packages BTagging.

## How it works

## Package Contents
JetTagEvent contains the following files/classes:
- JetTag ... the basic b jet object.
- JetTagContainer ... and its container for storegate
